﻿using System.Web;
using System.Web.Optimization;

namespace SmartStudentBackOffice
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js",
            //          "~/Scripts/respond.js",
            //          "~/Scripts/popper.js"
            //          ));

            bundles.Add(new StyleBundle("~/Content/SiteCss").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/dist/css/AdminLTE.min.css",
                      "~/Content/dist/css/skins/_all-skins.min.css"
                      ));
            bundles.Add(new ScriptBundle("~/bundles/SiteScripts").Include(
                     "~/Content/dist/js/adminlte.min.js",
                     "~/Content//dist/js/demo.js",
                     "~/Scripts/bootstrap.js"));

            BundleTable.EnableOptimizations = true;
        }
        
    }
}